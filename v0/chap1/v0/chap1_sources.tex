\section{Gravitational Waves Sources}
\label{sec:GWsources}

\vspace{2mm}

Due to the fact that gravitational waves present quadripolar radiation, the efficiency in converting mechanical energy in a system into gravitational radiation will be very low, making the signals produced by accelerating systems to be very weak. In practical terms, this means that the main sources of gravitational waves that are likely to be detected will be coming from astrophysical objects, as neutron stars or black holes, due to their potentially huge masses accelerating very strongly. \\

When we look at the spectrum of possible gravitational wave sources in the observable wave band, we see a range spanning over eight orders of magnitude in frequency. We are mainly interested in the sources that emit gravitational waves powerful enough to be detected by ground-based interferometric detectors as LIGO and Virgo, i.e. aiming to detect signals from sources radiating in the frequency range of $10$ to $10^4$ Hz.\\
Besides this constraint in frequency, a good source of gravitational waves is expected to be \textit{asymmetric}, \textit{compact} and \textit{relativistic}. The emitted power of the source is not the only decisive parameter while evaluating possible good sources, two additional aspects have to be taken into account. The first is the distance of the sources from the Earth, meaning that as the distance increases, the louder an event needs to be in order to be detected. The second is the rate at which the transient events occur, regarding its impact on the detection probability. \\
Once we have considered these different aspects, we can go all along to consider in more detail the possible sources of gravitational waves in which we shall be interested. These potential sources are usually organized into four main groups according to their gravitational wave emission type. The first to be considered will be compact binary coalescence, whose signals are produced by in-spiralling binary neutron stars or black holes. The second is continuous waves that may be produced by a rotating neutron star with time varying quadrupole moments. Thirdly, we consider the stochastic gravitational wave background due to the incoherent superposition of gravitational waves emitted by many sources. Finally, in the last part we consider possible bursts, or transients, signals corresponding to particular cataclysmic events.  \\

\subsection{Compact Binary Coalescence}

Compact binary coalescence signals are the primary source target of gravitational waves for ground based detectors. These systems are binaries composed of two Neutron Stars (BNSs), two Black Holes (BBHs) or a neutron star together with a black hole (NSBH).\\
As observed with the binary system PSR $1913+16$, they represent an ideal source for ground based gravitational wave detectors, as their compactness allows them to have an orbital separation increasingly small until their merge, leading to emission of gravitational waves throughout all the process. The gravitational wave signal will depend on the physical parameters of the binary system: masses, spins and eccentricity, and it presents three main parts, where the first is the in-spiral, which increases in frequency and amplitude as the compact objects move closer, together with the energy and angular momentum being carried away in gravitational waves. The second will be the merger when the black holes or neutron stars collide, and finally there is a ringdown phase. \\
Note that even if these systems spiral inwards over several billions of years, emitting gravitational waves, the only detectable signal by the ground base interferometric detector is the one from the last minutes (to a few seconds) before the merger, when the gravitational wave's frequency becomes sufficiently high and increasing rapidly in order to reach in the detector's observational sensitivity ($> 10$ Hz).\\
These signals can be used to test general relativity, specially in the strong-field regime, but also the post-Newtonian theory, and other theories of gravity. Moreover, measurements of orbital eccentricity and black hole spins can also provide significant insight into binary formation scenarios.


\subsection{Continuous Waves}

Continuous sources of gravitational waves are considered to be constantly emitting gravitational waves.\\
The primary expected source of continuous gravitational waves are rotating neutron stars with an assymetric mass distribution which are expected to have a time-varying quadrupole moment due to elastic or magnetic deformations, instabilities, and free precession. Such waves are not expected to be as loud as the gravitational waves generated from more violent transient events, therefore it is expected that ground based gravitational waves detectors may only be able to detect these sources within our galaxy. \\
Pulsars are a particular type of neutron stars, which emit a periodic radio pulse. They are especially interesting because some pulsars have been observed. Using these observations we can estimate the parameter-space which can be used to perform a more sensitive search.

\subsection{Stochastic Gravitational Wave Background}

The stochastic background of gravitational waves may be produced by the superposition of weak individually unresolvable cosmological and astrophysical sources.

Cosmological sources include quantum vacuum fluctuations during the inflationary epoch of the universe, electro-weak phase transitions, pre-big bang scenario in the context of string theory and cosmic strings. While astrophysical sources can be compact binary coalescence, supernoave, pulsar and many others.

Keeping in line with traditional cosmological definitions, the stochastic gravitational wave background is described by the gravitational wave energy density defined as

\begin{equation}
\Omega_{GW} (f) = \frac{f}{\rho_c} \frac{d \rho_{GW}}{df} \ ,
\end{equation}

where $\rho_c$ is the critical energy density of the universe and $d \rho_{GW}$ is the gravitational wave energy density contained in the frequency ranged $f$ to $f + df$.


\subsection{Gravitational Wave Bursts}

This predicted population of signals contains the gravitational waves transients, or bursts, emissions that have only a short duration compared to the observation time. The burst sources are in general not well understood, and in consequence not well modelled.\\
A typical example of these events comes from supernovae. Others include the collapse of a star to form a neutron star or a black hole, and binary black hole mergers. Other possible scenarios include the test of hypothetical sources as cosmic strings that will be discussed in an dedicated section.\\
Note that the number of possible sources has been increasing over the years and the parameter space now goes from very short duration signals to transient lasting a few minutes or days.  \\



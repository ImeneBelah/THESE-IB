\section{Gravitational Waves In Vacuum}
\label{sec:GRvacuum}

\subsection{Generalities}

In this section we will give the simplest solution to the Einstein weak-field equation. In the vacuum, the stress-energy tensor is zero and then the wave equation becomes 

\begin{equation}\label{waves-vacuum}
     \Box \bar{h}_{\alpha\beta}=0.
\end{equation}

The general solution can be written as a superposition of monochromatic progressive plane waves,

\begin{equation}\label{waves-plane}
  \bar{h}_{\alpha\beta}(x^{\mu})=A_{\alpha\beta}\exp (ik_{\mu}x^{\mu}), 
\end{equation}

where $A_{\alpha\beta}$ represents the amplitude wave, a constant and symmetrical matrix of dimension $4 \times 4$. Note that the amplitude matrix contains only $10$ independant components, due to the symmetrical propriety, while $k_{\mu}$ is the wavevector, a constant vector with component $(-\omega,k_{x},k_{y},k_{z})$, where  $\omega$ is the angular frequency. The wavevector determines the propagation direction of the wave and its frequency. 

This two quantities cannot be chosen arbitrarily as they must satisfy some conditions. By using the weak-field Einstein equation in vacuum Eq.\ref{waves-vacuum} one obtain a first condition, 

\begin{equation}\label{wave-vector-condition}
  k^{\alpha}k_{\alpha}=0 \Leftrightarrow  -\omega ^{2}+\left \|  \vec{k}\right \|^{2}=0
\end{equation}

which tell us that gravitational waves in vacuum move at the speed of light, as we remember that $c=1$. 

The second conditions is given by the Lorenz gauge Eq.\ref{lorenz-gauge}, 

\begin{equation}\label{vector-amplitude-condition}
  k_{\alpha}A^{\alpha\beta}=0
\end{equation}

which is the requirement that the effect of the gravitational waves is ortogonal to their direction of propagation. Moreover, this relation can be written as four equations that impose four conditions on $A_{\alpha\beta}$,  and this is the first step in reducing the number of its independent components. Instead of having $10$ independent components, the amplitude matrix has only 6 independent ones. Due the gauge freedom, an appropriate choice of gauge can reduce the independant component of $A_{\alpha\beta}$ even more. 

Indeed, one can also find a choice of infinetesimal coordinate system that satisfies the Lorenz gauge Eq.\ref{lorenz-gauge} and cancel some components of the amplitude matrix $A_{\alpha\beta}$. This choice is commonly known as the transverse-traceless gauge or TT gauge,

\begin{equation}\label{h-tt-conditions}
  \begin{split}
    \quad \textrm{transverse} \quad &\Rightarrow  \bar{h}_{0\alpha}=0\\ 
    \quad \textrm{traceless} \quad &\Rightarrow  \bar{h}=0 
  \end{split}
\end{equation}

Using the Eq.\ref{waves-plane}, we can also write these equations as

\begin{equation}\label{amplitude-conditions}
 \begin{split}
   \quad \textrm{transverse} \quad &\Rightarrow  A_{0 \alpha}=0\\
   \quad \textrm{traceless} \quad &\Rightarrow  A_{\alpha}^{\alpha}=0
 \end{split}
\end{equation}

which consequently add $4$ constraints on the amplitude components, $1$ from the traceless condition and only $3$ from the transverse condition, since there is a redundancy with one of the constraints
from the Lorenz gauge condition given in Eq.\ref{lorenz-gauge}. We have now reduced the independent degrees of freedom of the amplitude matrix $A_{\alpha\beta}$ to only $2$.

\subsection{Example}

In order to make it more clear, we provide an example here. We consider a gravitational wave with angular frequency $\omega$ that propagates along the z-axis. The wavevector is then given by

\begin{equation}
  k_{\beta}=(-\omega,0,0,+\omega).
\end{equation}

To impose constraints on the amplitude matrix $A_{\alpha\beta}$ we need the gauge equations. The Lorenz gauge, Eq.\ref{lorenz-gauge} implies that

\begin{equation}
   \omega(A_{\alpha 0}-A_{\alpha z}) =0, 
\end{equation}

while the transverse condition in the TT gauge fixes 

\begin{equation}
  A_{\alpha 0}=0.  
\end{equation}

Finally by taking into account that the amplitude matrix is symmetrical, the only non-zero term for $A_{\alpha\beta}$ are : $A_{xx}$, $A_{xy}=-A_{yx}$ and $A_{yy}$. But the traceless condition of the TT gauge give us an additional constraints,

\begin{equation}
  A_{xx}+A_{yy}=0. 
\end{equation}

This results in a wave amplitude matrix with only two independent components.

\begin{equation}
 \begin{split}
  A_{xx}=-A_{yy}=A_{+}\\
  A_{xy}=-A_{yx}=A_{\times}
  \end{split}
\end{equation}

The wave is a linear combination of two independant typs of waves, which we call polarizations of the gravitational wave. These polarization states are commonly refered to as ``plus`` and ``cross`` polarizations.

In that example, the solution of the weak-field Einstein equation in vacuum is written :

\begin{equation}\label{h-matrix}
  h_{\alpha\beta}=
  \begin{pmatrix}
    0 & 0 & 0 & 0\\ 
    0 & h_{+} & h_{\times} & 0\\ 
    0 & h_{\times} & -h_{+}& 0\\ 
    0 & 0 & 0 & 0
  \end{pmatrix}
\end{equation}

with

\begin{equation}\label{h-solution}
  \begin{split}
  h_{+}=A_{+}\exp[i\omega(t-z)]\\
  h_{\times}=A_{\times}\exp[i\omega(t-z)].
  \end{split}
\end{equation}

\subsection{Gravitational waves effect on matter }

In this section we want to understand the physical effect of a passing gravitational wave. We consider two particles A and B, moving freely, which is to say they are not subject to any forces, and a gravitational wave that propagates along the z-axis.

By using the metric seen in Eq.\ref{waves-plane} and satisfying the traceless-transverse gauge Eq.\ref{h-tt-conditions}, the distance between the two test masses is given by

\begin{equation}\label{distance-tt-gauge}
  ds^{2}=g_{\alpha\beta}dx^{\alpha}dx^{\beta}=-dt^{2}+(\delta _{ij}+h_{ij})dx^{i}dx^{j}
\end{equation}

where the $\delta _{ij}$ represents the Kronecker symbol. If we consider a coordinate system where the mass A is at the initial position $(-t, 0, 0, 0)$ and the masse B is at the position $(-t, x_{B}, y_{B}, z_{B})$ separated by a distance $L_{0}$, then the distance L, at any time, between the two masses is given by Eq.\ref{distance-tt-gauge} as

\begin{equation}
  L^{2}=\sqrt{(\delta _{ij}+h_{ij})dx^{i}dx^{j}}. 
\end{equation}

We can rewrite it under the form,

\begin{equation}
  L=L_{0}[(\delta_{ij}+h_{ij})n^{i}n^{j}]^{1/2}
\end{equation}

where we used $x_{B}^{i}=L_{0}n^{i}$ with $n^{i}$ the spatial vector between the test masses.

At first order, we obtain the relative variation of the distance L to the passage of a gravitational waves, 

\begin{equation}\label{distance-relative}
  \frac{\delta L}{L}=\frac{1}{2}h_{ij}n^{i}n^{j}, 
\end{equation}

which shows that the relative change in distance is proportional to the amplitude of the wave $h$, also called strain amplitude.

To simply visualize this effect we choose a new coordinate system defined by the transformation, 

\begin{equation}
\hat{x}^{i}(t)=x_{0}^{i}+\frac{1}{2}h_{ij}(t,0)x^{j}_{0}. 
\end{equation}

For a gravitational wave that propagates along the z-axis we gave the $h_{ij}$ component in Eq.\ref{h-matrix} and we have
\begin{equation}
  \begin{split}
  \hat{x}(t)&=x_{0}+\frac{1}{2}[A_{+}x_{0}+A_{\times}y_{0}]e^{i\omega t}\\
  \hat{y}(t)&=y_{0}+\frac{1}{2}[A_{\times}x_{0}-A_{+}y_{0}]e^{i\omega t}\\
  \hat{z}(t)&=z_{0}
  \end{split}
\end{equation}

We represent the deformation of a ring of test masses in the plan $z=0$ for both polarization. Qualitatively, a gravitational wave that is propagating perpendicularly to the plane of a ring of test particles will cause the ring to deform into an ellipse, first along one axis and then along the other, oscillating between these two configurations as a function of time.

\begin{figure}[t]
  \centering
  \includegraphics[width=0.7\columnwidth]{/home/belahcene/Documents/Thesis-IB/v0/chap1/v0/figures/pluscross.png}
  \caption{Effect of a passing gravitational wave propagating in the z=0 plan, with plus polarization (top) and cross polarization (bottom) on a ring of freely-falling particles.}
  \label{fig:gw-effect}
\end{figure}

 \section{General Relativity}
\markright{\thesection.~~Gravitational Waves}
\label{sec:GR}

\subsection{Geometry in general relativity}
In contrast to galilean mechanics, general relativity claims that time and space are interwoven into a single continuum known as ``space-time``. A ``point-event`` is a point in space, given by its three coordinates at an instant $t$ in time. In this description time and space variables must be expressed in the same unity, by convention we choose the meter. This can be done if we introduce the velocity of light $c$ and thus it takes the value of 1 and is dimensionless.
A point in space-time is then described by a four component vector,

\begin{equation}
 \vec{x}=\begin{pmatrix}
x^{0}\\ 
x^{1}\\ 
x^{2}\\ 
x^{3}\\
\end{pmatrix}=\begin{pmatrix}
t\\ 
x\\ 
y\\ 
z\\
\end{pmatrix}.
\end{equation}

It will be convenient to use a different notation, and write this vector as $x^{\mu}$, where $\mu$ is an index running from 0 to 3.

The geometry of a space is described by the metric, which defines the concept of distance between two points. In Euclidean geometry where the metric is given by the scalar product defined as positive, the square of the distance $dl$ between two infinitesimally close points could write

\begin{equation}
  dl^{2}=g_{ij}dx^{i}dx^{j}, 
\end{equation}

where the Einstein summation convention is used, and $g_{ij}$ is the metric given by 

\begin{equation}
 g_{ij}=\mathit{diag}(1,1,1)
\end{equation}

of signature $(+,+,+)$. In a similar way, the space-time interval in general relativity geometry is given by

\begin{equation}\label{distance}
ds^{2}=g_{\alpha\beta}dx^{\alpha}dx^{\beta}, 
\end{equation}

where the metric $g_{\alpha\beta}$ is a bilinear form, symmetrical and not degenerated of signature $(-,+,+,+)$.


\subsection{The Einstein Equation}

The properties of the gravitational field are encoded in the metric. More precisely, the Einstein equation is the fundamental equation in general relativity, and it determines the metric in function of the energy-impulsion distribution in the space-time, being given by

\begin{equation}\label{einstein}
  G_{\alpha\beta}=\kappa T_{\alpha\beta} .
\end{equation}

We are going to explain each of the terms of the Einstein equation. $G_{\alpha\beta}$ is called the Einstein tensor, in order to understand this term we need to introduce a mathematical tool, the affine connexion. In the Euclidean space, differentiation of vector fields is simply obtained by derivating the coordinates functions, since one has a constant basis. This is no longer true in the space-time. The affine connection is a geometric object which connect nearby tangent spaces, and so allows tangent vector fields to be differentiated. For a given metric $g_{\alpha\beta}$, one can show that the affine connexion is fixed, having for coefficient the Christoffel symbols of the metric, 

\begin{equation}\label{christofel}
  \Gamma ^{\alpha}_{\mu\nu}=\frac{1}{2}g^{\alpha\sigma}\left ( \frac{\partial g_{\sigma\nu}}{\partial x^{\mu}} + \frac{\partial g_{\mu\sigma}}{\partial x^{\nu}} - \frac{\partial g_{\mu\nu}}{\partial x^{\sigma}} \right).
\end{equation}

It is now straightforward to calculate the variation of a vector field between two points, by integrating even if in general the result depends on the choice of path, or in other words on the curvature of the affine connexion. The curvature is expressed trough derivatives of Christoffel symbols, but in a much more ”pleasant” form of a tensor called the Riemann tensor,

\begin{equation}\label{riemann}
  R^{\alpha}_{\beta\mu\nu}=\frac{\partial \Gamma ^{\alpha}_{\beta\nu} }{\partial x^{\mu}} + \frac{\partial \Gamma ^{\alpha}_{\beta\mu} }{\partial x^{\nu}}+\Gamma ^{\alpha}_{\sigma\mu}\Gamma ^{\sigma}_{\beta\nu}-\Gamma ^{\alpha}_{\sigma\nu}\Gamma ^{\sigma}_{\beta\mu}
\end{equation}

From this definition it comes two useful quantities that play an essential role for formulation of Einstein equations, the Ricci tensor $R_{\alpha\beta}$ define as :

\begin{equation}\label{ricci-tensor}
  R_{\alpha\beta}=R^{\sigma}_{\alpha\sigma\beta}
\end{equation}

and the trace of the Ricci tensor, also called the Ricci scalar $R$

\begin{equation}\label{ricci-scalar}
  R=g^{\alpha\beta}R_{\alpha\beta} .
\end{equation}

It allows us to define the Einstein tensor as 
\begin{equation}\label{einstein-tensor}
    G_{\alpha\beta}=R_{\alpha\beta} - \frac{1}{2}Rg_{\alpha\beta}.
\end{equation}
 
We understand now the left side of the Einstein equation decsribes the geometry of the space-time.

In general relativity, it is the matter that curve space time such that  we deduce the right side of the Einstein equation should describe the content in matter of space-time. Indeed, $T_{\alpha\beta}$ is a tensor, called the stress-energy tensor and it contains the information of the mass-energy and momentum density present in space-time.

There is still a term to be clarified in the Einstein equation, the $\kappa$ quantity. Being general relativity an extension of Newtonian gravity  the Einstein equation should in a classical system reduces to Newton's equations of gravity. When we look in classical theory, the distribution of matter of density  $\rho$ gives rise to a gravitational potential $\Phi$ which satisfies Poisson's equation

\begin{equation}\label{poisson}
  \nabla^{2}\Phi=4\pi G \rho, 
\end{equation}

where $G$ is Newton's constant.

If we consider a perfect fluid of density $\rho$ at rest, with nul pressure then we can show that the stress-energy tensor would have only one non-zero component $T_{00}=\rho$ and  that Einstein’s equation reduce to

\begin{equation}\label{newton-limit}
  2\nabla^{2}\Phi=\kappa \rho, 
\end{equation}

By identifying Eq.\ref{newton-limit} with Eq.\ref{poisson} we set

\begin{equation}
  \kappa=8\pi G 
\end{equation}

and find that general relativity thus passes its most important test: in agreeing with Newtonian gravity, in the classical limit.
